#!/bin/bash

#persistent backup
sudo docker run --rm --volumes-from jenkins -v $(pwd):/backup busybox tar cjvf backup/jenkins_home.tar.bz2 var/jenkins_home

#persistent restore

#sudo docker run --rm --volumes-from jenkins -v $(pwd):/backup busybox tar xjf /backup/jenkins_home.tar.bz2
