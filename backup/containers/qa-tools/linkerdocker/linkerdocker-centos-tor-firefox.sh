#!/bin/bash

target_container="tomcat"
target_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $target_container)

echo $target_ip $target_container >./conf/target

docker stop linkerdocker-centos-ff-tor
docker rm linkerdocker-centos-ff-tor

docker build -f dockerfile-centos-tor-firefox -t linkerdocker-centos-ff-tor . && docker run -d -it --privileged --rm  -p 9222:9222 -p 5920:5920 -p 5910:5910 --net=esnet \
-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
--mount type=bind,source="$(pwd)/conf/proxychains.conf",target=/etc/proxychains.conf \
--mount type=bind,source="$(pwd)/conf/target",target=/opt/target \
--mount type=bind,source="$(pwd)/internals/centos-firefox-useragent-addon.sh",target=/opt/bootstrap.sh \
--mount type=bind,source="$(pwd)/conf/torrc-centos",target=/etc/tor/torrc \
--mount type=bind,source="$(pwd)/addons/random_user_agent-2.2.10-an+fx.xpi",target=/opt/{b43b974b-1d3a-4232-b226-eaa2ac6ebb69\}.xpi \
--cap-add SYS_ADMIN --privileged \
--name linkerdocker-centos-ff-tor  linkerdocker-centos-ff-tor "/opt/bootstrap.sh" \

echo linkerdocker -ubuntu -tor firefox  up
echo host network bridged ip url for vnc:
echo http://$(hostname -I |awk '{print$2}'):5920
echo forwarded url for vnc:
echo http://127.0.0.1:5920
echo ...
echo linkerdocker -ubuntu -tor chrome  up
echo host network bridged ip url for vnc:
echo http://$(hostname -I |awk '{print$2}'):5910
echo forwarded url for vnc:
echo http://127.0.0.1:5910
