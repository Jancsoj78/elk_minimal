#!/bin/bash

target_container="tomcat"
target_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $target_container)

echo $target_ip $target_container >./conf/target

docker stop linkerdocker
docker rm linkerdocker

docker build -f dockerfile -t linkerdocker . && docker run -d -it --privileged --rm -p 5900:5900 --net=esnet \
--mount type=bind,source="$(pwd)/conf/target",target=/opt/target \
--mount type=bind,source="$(pwd)/internals/bootstrap.sh",target=/opt/bootstrap.sh \
--mount type=bind,source="$(pwd)/internals/urls.txt",target=/opt/urls.txt \
--mount type=bind,source="$(pwd)/addons/random_user_agent-2.2.10-an+fx.xpi",target=/opt/{b43b974b-1d3a-4232-b226-eaa2ac6ebb69\}.xpi \
--name linkerdocker linkerdocker sh "/opt/bootstrap.sh"

echo linkerdocker up
echo host network bridged ip url for vnc:
echo http://$(hostname -I |awk '{print$2}'):5900
echo forwarded url for vnc:
echo http://127.0.0.1:5900
