#!/bin/bash

tag="lts"
image="jenkins/jenkins"

docker stop jenkins
docker rm jenkins
docker volume create jenkins_data
docker run -d \
  -p 8080:8080 \
  -p 50000:50000 \
  --volume jenkins_data:/var/jenkins_home:Z \
  --volumes-from tomcat \
  --name=jenkins \
  --net=esnet \
  $image:$tag
  
echo jenkins up
echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):8080
echo forwarded url
echo http://127.0.0.1:8080

echo admin password:
echo $(docker exec -i -t jenkins /bin/cat /var/jenkins_home/secrets/initialAdminPassword)
