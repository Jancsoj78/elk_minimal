#! /bin/bash

image="docker.elastic.co/logstash/logstash"
tag="7.2.0"

docker stop logstash
docker rm logstash

docker run -d -it --rm  --net esnet \
-v /vagrant/pipeline/:/usr/share/logstash/pipeline/ \
-p 5044:5044 --name logstash $image:$tag

echo logstash up
echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):5044
echo forwarded url
echo http://127.0.0.1:5044


