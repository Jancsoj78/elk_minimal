#!/bin/bash

tag="7.2.0"
image="docker.elastic.co/beats/filebeat"

docker stop filebeat
docker rm filebeat
curl -H 'Content-Type: application/json' -XPUT 'http://localhost:9200/_ingest/pipeline/jvm_pipe' -d@../elasticsearch/jvm_pipe.json

docker run -d -it --rm --mount type=bind,source="$(pwd)/jvm_pipe.yml",target=/usr/share/filebeat/filebeat.yml \
--mount type=bind,source=/var/lib/docker/containers,target=/usr/share/dockerlogs/data,readonly \
--mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
--name filebeat --net esnet $image:$tag