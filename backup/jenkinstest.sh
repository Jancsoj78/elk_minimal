#!/bin/bash
#only jenkins test
./init.sh
cd ./containers/elasticsearch
cd ../jenkins
./jenkins.sh
cd ../qa-tools/selenium
./selenium.sh
cd ../../..
/bin/cp -f /etc/hosts /tmp/hosts-save
echo original host file is saved to /tmp/hosts-save
/bin/cp -f ./hosts /etc/hosts 

echo jenkinstest_stack is ready.

echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):8080
echo forwarded url
echo http://127.0.0.1:8080

