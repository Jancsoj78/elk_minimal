#!/bin/bash
#apm tomcat-apm-kibana-demo with qa tool
./init.sh
cd ./containers/elasticsearch
./elastic.sh
cd ../apm-server
./apmserver.sh
cd ../tomcat
./tomcat.sh
cd ../kibana
./kibana.sh
cd ../jenkins
./jenkins.sh
cd ../qa-tools/selenium
./selenium.sh
cd ../linkerdocker
./linkerdocker.sh
cd ../../..
/bin/cp -f /etc/hosts /tmp/hosts-save
echo original host file is saved to /tmp/hosts-save
/bin/cp -f ./hosts /etc/hosts 

echo stack 1 is ready.

echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):5601/app/apm#/apm-1/metrics?rangeFrom=now-1m&rangeTo=now
echo forwarded url
echo http://127.0.0.1:5601/app/apm#/apm-1/metrics?rangeFrom=now-1m&rangeTo=now




