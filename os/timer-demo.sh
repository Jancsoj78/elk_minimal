#!/bin/bash

current_epoch=$(date +%s)
target_epoch=$(date -d '07/05/2019 08:25' +%s)
sleep_seconds=$(( $target_epoch - $current_epoch ))

echo countdown to housekeeping...
until [ $sleep_seconds == 0 ]
        do
                current_epoch=$(date +%s)
                sleep_seconds=$(( $target_epoch - $current_epoch ))
                echo  -ne "$sleep_seconds"\\r
        done
echo House keeping...

#commands here

echo House keeping end.
#tomorrow
echo $(date --date=tomorrow +"%Y.%m.%d")
#before yesterday
echo $(date --date="-2 day" +"%Y.%m.%d")