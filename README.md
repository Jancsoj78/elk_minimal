# ELK_Minimal extended Stack Builder Scripts
There is some of **A versus B** Project base on **elasticsearch docker** images and nowadays popular tools to log analyzing , **metric** and **visualizing**

### Comparison projects 
  - elasticsearch integrated ingest versus logstash
  - 6.5 vs. 7.0 stack downgrade 7.0 to 6.5 (Grafana works 6.5)
  - kibana visual vs. grafana visual (canvas the best!)
  - filebeat vs. metricbeat vs. logng
  - centos host vs. atomic host
  - vm vs. baremetal
### CI/CD projects.
  - additional jenkins 
  - additional kafka
  - additional selenium