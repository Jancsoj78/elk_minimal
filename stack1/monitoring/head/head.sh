#!/bin/bash

docker stop head
docker rm head

docker run -d -it --rm --name head -p 9100:9100 --net esnet mobz/elasticsearch-head:5

echo elasticsearch head plugin
echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):9100
echo forwarded url
echo http://127.0.0.1:9100