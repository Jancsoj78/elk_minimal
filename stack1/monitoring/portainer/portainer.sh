#!/bin/bash
docker stop portainer
docker rm portainer
docker volume create portainer_data
docker run -d -p 9000:9000 --name portainer \
-v /var/run/docker.sock:/var/run/docker.sock \
-v portainer_data:/data portainer/portainer

#default password admin:admin

echo portainer up
echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):9000
echo forwarded url
echo http://127.0.0.1:9000
