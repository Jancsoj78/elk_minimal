#!/bin/bash

target_container="tomcat"
target_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $target_container)

echo $target_ip $target_container >./conf/target

docker stop linkerdocker-ubi-ff-tor
docker rm linkerdocker-ubi-ff-tor


docker build -f dockerfile-ubuntu-tor-firefox -t linkerdocker-ubi-ff-tor . && docker run -d -it --rm -p 5921:5921 -p 5911:5911 --net=esnet \
--mount type=bind,source="$(pwd)/conf/proxychains.conf",target=/etc/proxychains.conf \
--mount type=bind,source="$(pwd)/conf/target",target=/opt/target \
--mount type=bind,source="$(pwd)/internals/ubuntu-firefox-useragent-addon.sh",target=/opt/bootstrap.sh \
--mount type=bind,source="$(pwd)/conf/torrc-ubuntu",target=/usr/share/tor/tor-service-defaults-torrc \
--mount type=bind,source="$(pwd)/addons/random_user_agent-2.2.10-an+fx.xpi",target=/opt/{b43b974b-1d3a-4232-b226-eaa2ac6ebb69\}.xpi \
--name linkerdocker-ubi-ff-tor linkerdocker-ubi-ff-tor sh "/opt/bootstrap.sh"

echo linkerdocker -ubuntu -tor firefox  up
echo host network bridged ip url for vnc:
echo http://$(hostname -I |awk '{print$2}'):5921
echo forwarded url for vnc:
echo http://127.0.0.1:5921
echo ...
echo linkerdocker -ubuntu -tor chrome  up
echo host network bridged ip url for vnc:
echo http://$(hostname -I |awk '{print$2}'):5911
echo forwarded url for vnc:
echo http://127.0.0.1:5911