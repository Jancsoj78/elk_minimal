#!/bin/bash


docker stop selenium-hub
docker rm selenium-hub


docker run -d -p 4444:4444 --net esnet --name selenium-hub selenium/hub:3.4.0
docker run -d -v --net esnet /dev/shm:/dev/shm --link --name selenium-chrome001 selenium-hub:hub selenium/node-chrome:3.4.0
docker run -d -v --net esnet /dev/shm:/dev/shm --link --name selenium-firefox001 selenium-hub:hub selenium/node-firefox:3.4.0
