#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

tag="7.2.0"
image="docker.elastic.co/elasticsearch/elasticsearch"

docker stop elasticsearch
docker rm elasticsearch

docker volume rm esdata
docker volume create --name esdata
docker network rm esnet
docker network create esnet

docker run -d -p 9200:9200 \
-e "discovery.type=single-node" \
-e "cluster.name=docker-cluster" \
-e "bootstrap.memory_lock=true" \
-e "http.cors.allow-origin=*" \
-e ES_JAVA_OPTS="-Xms1024m -Xmx1024m" \
-e ES_JAVA_OPTS="-Xlog:gc*" \
-e ES_JAVA_OPTS="-XX:+PrintGCDetails" \
-e "xpack.monitoring.collection.enabled=true" \
-v esdata:/usr/share/elasticsearch/data \
--ulimit memlock=-1:-1 \
--net esnet \
--name elasticsearch \
$image:$tag


echo "1 elasticsearch on localhost:9200"

#  http://localhost:9200/*
# readable all about containers
  chmod -R 777 /var/lib/docker/containers/*

# wait for port alive
  now=$(date +"%T")
    echo -ne "Starting time : $now "
    echo waiting for port 9200 ...

until ncat -z 127.0.0.1 9200;
do
    now=$(date +"%T")
    echo -ne "Current time  : $now \r".

    sleep 1
done
echo "portcheck OK"

until [[ $(curl -s http://elasticsearch:9200/_cluster/health | jq .status) ]]; do printf '.'; sleep 10; done;

echo "healthcheck OK" 

# parsing log with elastic_ingest API
curl -H 'Content-Type: application/json' -XPUT 'http://localhost:9200/_ingest/pipeline/jvm_pipe' -d@jvm_pipe.json
echo "parsing OK"

echo elasticsearch  up
echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):9200
echo forwarded url
echo http://127.0.0.1:9200



