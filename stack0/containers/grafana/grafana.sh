#!/bin/bash
#  grafana

tag="6.5"
image="grafana/grafana"


docker stop grafana
docker rm grafana
sleep 5
docker run -d -it --rm -p 3000:3000 \
--mount type=bind,source="$(pwd)/defaults.ini",target=/usr/share/grafana/conf/defaults.ini \
--name grafana -e "GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-simple-json-datasource,bessler-pictureit-panel" \
--net esnet $image:$tag

# wait for port alive
now=$(date +"%T")
echo -ne "Starting time : $now "
echo waiting for port 3000 ...
until ncat -z 127.0.0.1 3000;
    do
	now=$(date +"%T")
	echo -ne "Current time  : $now \r".
	sleep 1
    done
echo "portcheck OK"
#waiting for grafana up healthcheck
#

until $(curl --output /dev/null --silent --head --fail http://localhost:3000/api/health); do
    printf '.'
    sleep 5
done
echo "healthcheck OK"

#  echo "6 grafana on localhost:80 redirected admin:admin"
# set dashboard and datasource via grafana API

curl -X "POST" "http://localhost:3000/api/datasources" -H "Content-Type: application/json" --user admin:admin --data-binary @datasource.json
# add dashboard
curl -i -u admin:admin -H "Content-Type: application/json" -X POST http://localhost:3000/api/dashboards/db -d@dashboard_up.json
# export dashboard
curl -i -u admin:admin -H "Content-Type: application/json" -X POST http://localhost:3000/api/dashboards/import -d@dashboard.json
# set home dashboard
curl -i -u admin:admin -H "Content-Type: application/json" -XPUT localhost:3000/api/org/preferences -d@dashboard_home.json
# exporting dashbord use chrome developer export curl option..


echo grafana up
echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):3000
echo forwarded url
echo http://127.0.0.1:3000