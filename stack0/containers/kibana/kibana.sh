#!/bin/bash

tag="7.2.0"
image="kibana"

docker stop kibana
docker rm kibana
docker run -d -e " SERVER_NAME: kibana" \
-e "ELASTICSEARCH_URL: http://elasticsearch:9200" \
--mount type=bind,source="$(pwd)/kibana.yml",target=/usr/share/kibana/config/kibana.yml \
--name kibana --net esnet -p 5601:5601 $image:$tag

# wait for port alive

now=$(date +"%T")

echo -ne "Starting time : $now "
echo waiting for port 5601 ...

until ncat -z 127.0.0.1 5601;
    do
	now=$(date +"%T")
	echo -ne "Current time  : $now \r".
	sleep 1
    done

echo "portcheck OK"

until [[ $(curl -s http://kibana:5601/api/status | jq .status.overall.state) ]]; do printf '.'; sleep 10; done;

echo "healthcheck OK"


#import index (filebeat-*)
#kbn-version:7.2.0 
#curl --verbose -POST http://localhost:5601/api/saved_objects/index-pattern -H "kbn-version: 7.2.0" -H 'Content-Type: application/json;charset=UTF-8' -H 'User-Agent: Mozilla/5.0' -H 'Accept: Application/json, text/plain, /' -H 'DNT:1' -d '{"attributes":{"title":"filebeat-*","timeFieldName":"@timestamp","notExpandable":true}}'
#import index (.monitoring-es-*)
#curl --verbose -POST http://localhost:5601/api/saved_objects/index-pattern -H "kbn-version: 7.2.0" -H 'Content-Type: application/json;charset=UTF-8' -H 'User-Agent: Mozilla/5.0' -H 'Accept: Application/json, text/plain, /' -H 'DNT:1' -d '{"attributes":{"title":".monitoring-es-*","timeFieldName":"@timestamp","notExpandable":true}}'
#import index apm (apm)
curl --verbose -POST http://localhost:5601/api/saved_objects/index-pattern -H "kbn-version: 7.2.0" -H 'Content-Type: application/json;charset=UTF-8' -H 'User-Agent: Mozilla/5.0' -H 'Accept: Application/json, text/plain, /' -H 'DNT:1' -d '{"attributes":{"title":".apm-*","timeFieldName":"@timestamp","notExpandable":true}}'

echo "importing indexes OK"

echo kibana up
echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):5601
echo forwarded url
echo http://127.0.0.1:5601

