#!/bin/bash

# start webapps
docker stop tomcat
docker rm tomcat

docker run -d -it --mount type=bind,source="$(pwd)/wars",target=/opt/tomcat/webapps/ \
--mount type=bind,source="$(pwd)/conf",target=/opt/tomcat/conf/ \
--mount type=bind,source="$(pwd)/apm",target=/opt/tomcat/apm/ \
--net=esnet \
--name tomcat --rm -p 8888:8080 chenmins/tomcat-centos:jdk8tomcat8
sleep 5
docker exec tomcat /opt/tomcat/bin/catalina.sh run &
sleep 10
docker exec tomcat java -jar /opt/tomcat/apm/apm-agent-attach-1.6.1.jar -i 'org.apache.catalina.startup.Bootstrap' -c --args 'service_name=apm-1;server_urls=http://apmserver:8200' &
sleep 5

echo tomcat up
echo host network bridged ip url
echo http://$(hostname -I |awk '{print$2}'):8888
echo forwarded url
echo http://127.0.0.1:8888



