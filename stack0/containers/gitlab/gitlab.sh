#!/bin/bash

tag="latest"
#image="gitlab/gitlab-ce"
image="gitlab/gitlab-ee"

docker stop gitlab
docker rm gitlab
docker volume create gitlab_data
docker volume create gitlab_log

sudo docker run --detach \
  --hostname gitlab.example.com \
  --publish 443:443 --publish 80:80 --publish 22100:22 \
  --name gitlab \
  --restart always \
  --net=esnet \
  --volume $(pwd)/config:/etc/gitlab:Z \
  --volume gitlab_log:/var/log/gitlab:Z \
  --volume gitlab_data:/var/opt/gitlab:Z \
  $image:$tag

echo gitlab up
echo host network bridged ip url
echo https://$(hostname -I |awk '{print$2}'):443
echo forwarded url
echo https://127.0.0.1:443

