#!/bin/bash

tag="7.2.0"
image="docker.elastic.co/apm/apm-server"

docker stop apmserver
docker rm apmserver
curl -L -O https://raw.githubusercontent.com/elastic/apm-server/7.2/apm-server.docker.yml
docker run -d \
  -p 8200:8200 \
  --name=apmserver \
  --user=apm-server \
  --net=esnet \
  --volume="$(pwd)/apm-server.docker.yml:/usr/share/apm-server/apm-server.yml:ro" \
  $image:$tag \
  --strict.perms=false -e \
  -E output.elasticsearch.hosts=["elasticsearch:9200"]  
