#!/bin/bash


docker stop node02-ff node01-chrome selenium-hub 
docker rm node02-ff node01-chrome selenium-hub

#use --net instead --link

docker run -d -p 4444:4444 --net=esnet --name selenium-hub selenium/hub:3.4.0
docker run -d --net=esnet -e HUB_PORT_4444_TCP_ADDR=hub -e HUB_PORT_4444_TCP_PORT=4444 --name node01-chrome -v /dev/shm:/dev/shm --link selenium-hub:hub selenium/node-chrome:3.4.0
docker run -d --net=esnet -e HUB_PORT_4444_TCP_ADDR=hub -e HUB_PORT_4444_TCP_PORT=4444 --name node02-ff -v /dev/shm:/dev/shm --link selenium-hub:hub selenium/node-firefox:3.4.0

echo selenium grid
echo host network bridged ip url:
echo http://$(hostname -I |awk '{print$2}'):4444/grid/console
echo forwarded url:
echo http://127.0.0.1:4444/grid/console
